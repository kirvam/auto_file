use strict;
use warnings;
use Data::Dumper;
use Net::SMTP;


my $file = shift;

my($header_name_ref,$header_num_ref,$data_ref,$detail_ref) = read_file($file);

print Dumper \$header_name_ref;
print Dumper \$header_num_ref;
print Dumper \$data_ref;
print Dumper \$detail_ref;

print_hash_ref($data_ref);
print_hash_ref_id($detail_ref,'ID');

print "${$header_num_ref}{2}\n";

print_hash_ref_id($detail_ref,${$header_num_ref}{2});
print_hash_ref_id($detail_ref,${$header_num_ref}{4});



###
# subs
###
sub print_hash_ref_id {
my($hash_ref,$val) = @_;
print "---< printing list $val 's >-------\n";
 foreach my $key ( sort keys %{ $hash_ref } ){
    print "key: $key: ";
        print "${$hash_ref}{$key}->{$val},";
        print "\n";
  };
print "---< done >----\n";
};

sub print_hash_ref {
my($hash_ref) = @_;
 foreach my $key ( keys %{ $hash_ref } ){
    print "key: $key: ";
     foreach my $ii ( 0 .. $#{ ${$hash_ref}{$key}} ){
        print "${$hash_ref}{$key}[$ii],";
    };
        print "\n";
  };
};

sub read_file {
my($file) = @_;
my $ii = 0;
my @header = ();
my %header_name;
my %header_num;
my %data;
my %detail;
open( my $fh, "<", $file ) || die "Flaming death on open of $file: $!\n";
while(<$fh>){
   my $line = $_;
   chomp($line);
   $ii++;
   if( $line =~ m/^#{2,4}\s.*$/g ) {
       print "$ii: Found Header: $line\n";
       $line =~ s/#{2,4}\s//g;
       @header = split(/,\s{0,1}/,$line);
       foreach my $item ( 0 .. $#header ){
               print "$item of $#header is $header[$item]\n";
               $header_name{$header[$item]} = $item;
               $header_num{$item} = $header[$item];
          }; 
     } else {
       print "$ii: $line\n";
       my @array =  split(/,\s{0,1}/,$line);
       my $tag = $array[0];
       foreach my $item ( 0 .. $#header ){
           $detail{$tag}{$header[$item]} = @array[$item];
         };
       $data{$tag} = [ @array ];
       
  };
 };
      print "Total line: $ii\n";
 return(\%header_name,\%header_num,\%data,\%detail);
};
